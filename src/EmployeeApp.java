import java.util.ArrayList;

public class EmployeeApp {
	public static void main(String[] args) {
		ArrayList<Payable> alp = new ArrayList<>();
		alp.add(new SalariedEmployee("01", "ani", 1000000));
		alp.add(new HourlyEmployee("02", "budi", 20000, 40));
		alp.add(new SalariedEmployee("03", "charli", 1200000));
		alp.add(new HourlyEmployee("04", "dedi", 25000, 40));
		System.out.println(getHighestPaidEmployee(alp)); //output: charli
		System.out.println(getTotalHourlyEmployeeSalary(alp)); //output: 1800000.0
		
	}
	
	public static String getHighestPaidEmployee(ArrayList<Payable> al){
		if(al.isEmpty()){
			return "kosong nih";
		} else{
			Payable maximumPayableEmployee = al.get(0);
			for(Payable employee : al){
				if(employee.getPaymentAmount() > maximumPayableEmployee.getPaymentAmount()){
					maximumPayableEmployee = employee;
				}
			}
			return ((Employee)maximumPayableEmployee).getName();
		}
	}
	
	public static double getTotalHourlyEmployeeSalary(ArrayList<Payable> al){
		double total = 0;
		for(Payable employee : al){
			if(employee instanceof HourlyEmployee){
				total += employee.getPaymentAmount();
			}
			
		}
		return total;
	}
}

interface Payable{
	public double getPaymentAmount();
}

abstract class Employee implements Payable{
	private String id;
	private String name;
	
	public Employee(String id, String name){
		this.name = name;
		this.id = id;
	}
	
	public Employee(String id){
		this.id = id;
	}
	
	public String getName(){
		return name;
	}
	public String toString(){
		return id+" "+name;
	}
}


class SalariedEmployee extends Employee{
	private double weeklySalary;
	
	public SalariedEmployee(String id, String name, double weeklySalary) {
		super(id, name);
		this.weeklySalary = weeklySalary;
	}
	

	@Override
	public double getPaymentAmount() {
		return weeklySalary;
	}
}

class HourlyEmployee extends Employee{
	private double wage;
	private double hours;
	public HourlyEmployee(String id, String name, double wage, double hours) {
		super(id,name);
		this.wage = wage;
		this.hours = hours;
	}

	@Override
	public double getPaymentAmount() {
		double salary = 0;
		if(hours > 40){
			salary = 40 * wage;
			salary += (hours-40)*(wage*1.5);
		} else {
			salary = hours * wage;
		}
		return salary;
	}
	
}