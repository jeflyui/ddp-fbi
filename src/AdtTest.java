import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

public class AdtTest {
	public static void main(String[] args) {
		
		/*List list = new ArrayList<>();
		list.add(3);
		//list.set(0, 7);
		list.add(0,1);
		list.add(10);
		
		list.add(2);
		
		//Collections.sort(list);
		
		for(int i = list.size()-1; i>=0;i--){
			System.out.println(list.get(i));
		}*/
		
		////////////////////////////////////////////////////////
		
		/*Queue<Mahasiswa> queue = new PriorityQueue<>();
		queue.add(new Mahasiswa("1402", 20));
		queue.add(new Mahasiswa("1401", 20));
		queue.add(new Mahasiswa("1601", 1));
		queue.add(new Mahasiswa("1602", 11));
		
		while(!queue.isEmpty()){
			System.out.println(queue.poll().npm);
		}*/
		
		
		/*Set<Integer> set = new HashSet<>();
		set.add(13);
		set.add(13);
		set.add(1);
		set.add(1);
		set.add(11);
		
		
		Iterator itr = set.iterator();
		while(itr.hasNext()){
			System.out.println(itr.next());
		}*/
		
		
		/*Map<String, Mahasiswa> map = new TreeMap<>();
		map.put("juan",new Mahasiswa("1601", 30));
		map.put("tie",new Mahasiswa("1602", 12));
		Mahasiswa m = map.get("juan");
		//System.out.println(m.npm);
		
		Set<String> key = map.keySet();
		Iterator itr = key.iterator();
		
		while(itr.hasNext()){
			System.out.println(itr.next());
		}*/
	}
}



interface BangunDatar {
	public double getLuas();
	public double getKeliling();
}

class Segitiga implements BangunDatar, Comparable<BangunDatar>{
	double sisi1;
	double sisi2;
	double sisi3;
	
	public int compareTo(BangunDatar b){
		if(this.getLuas() < b.getLuas()){
			return -1;
		} else if(this.getLuas() > b.getLuas()){
			return 1;
		} else {
			return 0;
		}
	}
	
	public Segitiga(double sisi1, double sisi2, double sisi3) {
		this.sisi1 = sisi1;
		this.sisi2 = sisi2;
		this.sisi3 = sisi3;
	}
	
	public double getLuas(){
		return 10;
	}
	public double getKeliling(){
		return sisi1+sisi2+sisi3;
	}
}

class PersegiPanjang implements BangunDatar{
	double panjang;
	double lebar;
	
	
	public PersegiPanjang(double panjang, double lebar) {
		this.panjang = panjang;
		this.lebar = lebar;
	}
	
	public double getLuas(){
		return panjang*lebar;
	}
	public double getKeliling(){
		return 2*(panjang+lebar);
	}
}





class Mahasiswa implements Comparable<Mahasiswa>{
	String npm;
	int umur;
	
	public Mahasiswa(String npm, int umur){
		this.npm = npm;
		this.umur = umur;
	}
	
	// negatif lebih kiri
	// positif lebih kanan
	public int compareTo(Mahasiswa other){
		if(this.umur < other.umur){
			return -1;
		} else if(this.umur > other.umur){
			return 1;
		} else{
			return this.npm.compareTo(other.npm);
		}
	}
}


