import java.io.FileInputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeMap;

public class FileIOTest {
	public static void main(String[] args) throws IOException{
		Scanner s = new Scanner(new FileInputStream("input.txt"));
		//Scanner s = new Scanner(System.in);
		
		Map<String, Integer> map = new TreeMap<>();
		while(s.hasNextLine()){
			String inputLine = s.nextLine().toLowerCase();
			String[] strSplit = inputLine.split(" ");
			for(String str : strSplit){
				if(map.containsKey(str)){
					int i = map.get(str)+1;
					map.put(str, i);
				} else{
					map.put(str, 1);
				}
			}
		}
		
		Set<String> keyset = map.keySet();
		Iterator<String> itr = keyset.iterator();
		while(itr.hasNext()){
			String key = itr.next();
			System.out.printf("%-16s%d%n", key,map.get(key));
		}
	}
}
